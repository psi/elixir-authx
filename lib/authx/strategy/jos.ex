defmodule Authx.Strategy.Jos do
  import Plug.Conn
  import Authx, only: [redirect_to: 2]

  alias Authx.Client

  @authorize_url "https://oauth.jd.com/oauth/authorize"
  @token_url "https://oauth.jd.com/oauth/token"

  @authorize_params %{response_type: "code"}
  @access_token_params %{grant_type: "authorization_code"}
  @provider "jos"

  # http://jos.jd.com/doc/channel.htm?id=152
  def get_authorize_url(callback_url, params, state) do
    # "https://oauth.jd.com/oauth/authorize?response_type=code&client_id=YOUR_CLIENT_ID&redirect_uri=YOUR_REGISTERED_REDIRECT_URI&state=YOUR_CUSTOM_CODE"
    {client_id, _} = state

    query_params =
      @authorize_params
      |> Map.put(:client_id, client_id)
      |> Map.put(:redirect_uri, callback_url)
      |> Map.merge(params)
      |> URI.encode_query()

    @authorize_url <> "?" <> query_params
  end

  def request_phase(conn, _opts, state) do
    params = Map.take(conn.params, ["state"])

    authorize_url =
      conn
      |> get_callback_url()
      |> get_authorize_url(params, state)

    conn
    |> redirect_to(authorize_url)
    |> halt
  end

  def callback_phase(conn, opts, state) do
    authx =
      if conn.params["code"] do
        do_callback(conn, opts, state)
      else
        state_params = conn.params["state"]

        %Authx{
          provider: @provider,
          state: state_params,
          status: "FAIL",
          error: %{errcode: 401, errmsg: "no code"}
        }
      end

    conn
    |> put_private(:authx, authx)
  end

  def do_callback(conn, _opts, state) do
    state_params = conn.params["state"]

    access_token_info =
      conn
      |> get_access_token_url(state)
      |> get_access_token_info

    if Map.has_key?(access_token_info, :access_token) do
      uid = access_token_info[:uid]

      credentials =
        access_token_info
        |> get_credentials

      info =
        access_token_info
        |> get_info

      %Authx{
        provider: @provider,
        uid: uid,
        info: info,
        credentials: credentials,
        extra: %{access_token_info: access_token_info},
        state: state_params,
        status: "SUCCESS"
      }
    else
      %Authx{provider: @provider, state: state_params, status: "FAIL", error: access_token_info}
    end
  end

  def get_info(user_info) do
    %{
      nickname: user_info[:user_nick]
    }
  end

  def get_access_token_info(url) do
    case Client.get(url) do
      {:ok, body} -> body
      {:error, error} -> error
    end
  end

  def get_credentials(access) do
    time =
      access[:time]
      |> String.slice(0..9)
      |> String.to_integer()

    %{
      token: access[:access_token],
      refresh_token: access[:refresh_token],
      expires_at: time + access[:expires_in],
      scope: access[:scope]
    }
  end

  def get_access_token_url(conn, state) do
    # url	= "https://oauth.jd.com/oauth/token?grant_type=authorization_code&client_id=YOUR_CLIENT_ID&redirect_uri=YOUR_REGISTERED_REDIRECT_URI&code=GET_CODE&state=YOUR_CUSTOM_CODE&client_secret= YOUR_APP_SECRET"
    code = conn.params["code"]

    {client_id, secret} = state
    callback_url = get_callback_url(conn)

    query_params =
      @access_token_params
      |> Map.merge(%{
        client_id: client_id,
        client_secret: secret,
        code: code,
        redirect_uri: callback_url
      })
      |> URI.encode_query()

    @token_url <> "?" <> query_params
  end

  defp get_callback_url(conn) do
    conn
    |> Authx.get_url_with_path("/auth/#{@provider}/callback")
  end
end
