defmodule Authx.Strategy.Wechat do
  import Plug.Conn
  import Authx, only: [redirect_to: 2]

  alias Authx.Client

  require Logger

  @authorize_url "https://open.weixin.qq.com/connect/oauth2/authorize"
  @token_url "https://api.weixin.qq.com/sns/oauth2/access_token"

  @authorize_params %{response_type: "code", scope: "snsapi_userinfo"}
  @access_token_params %{grant_type: "authorization_code"}
  @provider "wechat"

  # https://mp.weixin.qq.com/wiki/17/c0f37d5704f0b64713d5d2c37b468d75.html
  def get_authorize_url(callback_url, params, state) do
    # "https://open.weixin.qq.com/connect/oauth2/authorize?appid=appid&redirect_uri=callback_url&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect"
    {client_id, _} = state

    query_params =
      @authorize_params
      |> Map.put(:appid, client_id)
      |> Map.put(:redirect_uri, callback_url)
      |> Map.merge(params)
      |> URI.encode_query()

    @authorize_url <> "?" <> query_params <> "#wechat_redirect"
  end

  def request_phase(conn, _opts, state) do
    params = Map.take(conn.params, ["state"])

    authorize_url =
      conn
      |> get_callback_url()
      |> get_authorize_url(params, state)

    conn
    |> redirect_to(authorize_url)
    |> halt
  end

  def callback_phase(conn, opts, state) do
    authx =
      if conn.params["code"] do
        do_callback(conn, opts, state)
      else
        state_params = conn.params["state"]

        %Authx{
          provider: @provider,
          state: state_params,
          status: "FAIL",
          error: %{errcode: 401, errmsg: "no code"}
        }
      end

    conn
    |> put_private(:authx, authx)
  end

  def do_callback(conn, _opts, state) do
    state_params = conn.params["state"]

    access_token_info =
      conn
      |> get_access_token_url(state)
      |> get_access_token_info()

    if Map.has_key?(access_token_info, :openid) do
      openid = access_token_info[:openid]

      credentials =
        access_token_info
        |> get_credentials()

      user_info =
        access_token_info[:access_token]
        |> get_userinfo_url(openid)
        |> get_userinfo()

      if Map.has_key?(user_info, :openid) do
        info =
          user_info
          |> get_info

        %Authx{
          provider: @provider,
          uid: openid,
          info: info,
          credentials: credentials,
          extra: %{user_info: user_info, access_token_info: access_token_info},
          state: state_params,
          status: "SUCCESS"
        }
      else
        %Authx{provider: @provider, state: state_params, status: "FAIL", error: user_info}
      end
    else
      %Authx{provider: @provider, state: state_params, status: "FAIL", error: access_token_info}
    end
  end

  def get_access_token_info(url) do
    case Client.get(url) do
      {:ok, body} -> body
      {:error, error} -> error
    end
  end

  def get_credentials(access) do
    %{
      token: access[:access_token],
      refresh_token: access[:refresh_token],
      expires_at: :erlang.system_time(:seconds) + access[:expires_in],
      scope: access[:scope]
    }
  end

  def get_access_token_url(conn, state) do
    # url	= "https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx5ae5c8bc5c80dd4b&secret=d35e56dc3d0a9db21c9ba2362a947d1b&code=#{code}&grant_type=authorization_code"
    code = conn.params["code"]

    {client_id, secret} = state

    query_params =
      @access_token_params
      |> Map.merge(%{appid: client_id, secret: secret, code: code})
      |> URI.encode_query()

    @token_url <> "?" <> query_params
  end

  def get_userinfo_url(access_token, openid) do
    "https://api.weixin.qq.com/sns/userinfo?access_token=#{access_token}&openid=#{openid}&lang=zh_CN"
  end

  def get_userinfo(url) do
    case Client.get(url) do
      {:ok, body} -> body
      {:error, error} -> error
    end
  end

  def get_info(user_info) do
    %{
      openid: user_info[:openid],
      unionid: user_info[:unionid],
      nickname: user_info[:nickname],
      sex: user_info[:sex],
      province: user_info[:province],
      city: user_info[:city],
      country: user_info[:country],
      headimgurl: user_info[:headimgurl]
    }
  end

  defp get_callback_url(conn) do
    conn
    |> Authx.get_url_with_path("/auth/#{@provider}/callback")
  end
end
