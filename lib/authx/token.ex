defmodule Authx.Token do
  use Joken.Config

  @impl true
  def token_config do
    default_claims(skip: [:iss, :nbf, :aud, :exp])
  end

  def make_token(uid, extra \\ %{}) do
    signer = Joken.Signer.create("HS256", get_secret())

    %{uid: uid}
    |> Map.merge(extra)
    |> generate_and_sign!(signer)
  end

  def decode_token(token) do
    signer = Joken.Signer.create("HS256", get_secret())

    case verify_and_validate(token, signer) do
      {:ok, claims} -> claims
      _ -> nil
    end
  end

  def get_secret() do
    Application.get_env(:authx, :token)
    |> Keyword.get(:secret)
  end
end
