defmodule Authx.Plug.Pipeline do
  defmacro __using__(_) do
    quote do
      @providers Application.get_env(:authx, :providers, [])
                 |> Enum.map(fn {_, v} -> v |> Keyword.get(:name) end)

      @strategies Application.get_env(:authx, :providers, [])
                  |> Enum.map(fn {_, v} ->
                    {v |> Keyword.get(:name) |> String.to_atom(), v |> Keyword.get(:module)}
                  end)

      def init(options), do: options

      def call(%Plug.Conn{path_info: ["auth", provider]} = conn, opts)
          when provider in @providers do
        conn
        |> do_request_phase(provider, opts)
      end

      def call(%Plug.Conn{path_info: ["auth", provider, "callback"]} = conn, opts)
          when provider in @providers do
        conn
        |> do_callback_phasek(provider, opts)
      end

      def call(conn, _opts) do
        conn
      end

      def do_request_phase(conn, provider, opts) do
        strategy = get_strategy(provider)

        state = {get_client_id(provider, conn), get_secret(provider, conn)}

        conn
        |> strategy.request_phase(opts, state)
      end

      def do_callback_phasek(conn, provider, opts) do
        strategy = get_strategy(provider)
        state = {get_client_id(provider, conn), get_secret(provider, conn)}

        conn
        |> strategy.callback_phase(opts, state)
      end

      def get_strategy(provider) do
        provider = String.to_atom(provider)

        @strategies
        |> Keyword.get(provider)
      end

      def get_client_id(provider, _conn) do
        provider = String.to_atom(provider)

        Application.get_env(:authx, :providers)
        |> Keyword.get(provider)
        |> Keyword.get(:client_id)
      end

      def get_secret(provider, _conn) do
        provider = String.to_atom(provider)

        Application.get_env(:authx, :providers)
        |> Keyword.get(provider)
        |> Keyword.get(:secret)
      end

      defoverridable get_client_id: 2, get_secret: 2
    end
  end
end
