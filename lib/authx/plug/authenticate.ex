defmodule Authx.Plug.Auth do
  import Plug.Conn

  alias Authx.Token

  def authenticating(conn, _options) do
    case get_uid(conn) do
      nil -> conn
      uid -> put_private(conn, :_xuid, uid)
    end
  end

  def authenticate(conn, _options) do
    case get_uid(conn) do
      nil -> conn |> send_resp(401, "unauthorized") |> halt()
      uid -> put_private(conn, :_xuid, uid)
    end
  end

  def get_uid(conn) do
    with %{_xtoken: token} <- conn.private,
        %{"uid" => uid} <- Token.decode_token(token) do
          uid
    else
      _ ->
        nil
    end
  end
end
