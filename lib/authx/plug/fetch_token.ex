defmodule Authx.Plug.FetchToken do
  import Plug.Conn

  def init([]), do: :cookie

  def init(default), do: default

  def call(conn, default) do
    case get_token(conn, default) do
      token when byte_size(token) > 0 ->
        put_private(conn, :_xtoken, token)

      _ ->
        conn
    end
  end

  defp get_token(conn, :cookie) do
    conn.cookies["_xtoken"]
  end

  defp get_token(conn, :bearer) do
    conn
    |> get_req_header("authorization")
    |> get_token_from_bearer
  end

  defp get_token_from_bearer(["Bearer " <> token]), do: token
  defp get_token_from_bearer(_), do: nil
end
