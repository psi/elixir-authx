defmodule Authx do
  defstruct provider: "",
            uid: "",
            info: nil,
            credentials: nil,
            extra: nil,
            state: "",
            status: "",
            error: nil

  import Plug.Conn, only: [resp: 3, halt: 1, put_resp_header: 3, put_resp_cookie: 3]

  def get_url_with_path(conn, path) do
    host =
      case Plug.Conn.get_req_header(conn, "host") do
        [] -> ""
        [host1] -> host1
      end

    scheme =
      case Plug.Conn.get_req_header(conn, "x-forwarded-proto") do
        [] -> to_string(conn.scheme)
        [scheme1] -> scheme1
      end

    %URI{authority: host, host: conn.host, scheme: scheme, port: conn.port, path: path}
    |> to_string
  end

  def redirect_to(conn, url) do
    conn
    |> put_x_redirect_url()
    |> put_resp_header("location", url)
    |> resp(302, "")
    |> halt
  end

  def put_x_redirect_url(conn, url \\ nil) do
    url = url || conn.params["redirect_url"]

    if url do
      conn
      |> put_resp_cookie("_x_redirect_url", url)
    else
      conn
    end
  end
end
